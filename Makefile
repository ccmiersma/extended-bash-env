all: build

build:  src/extended-bash-env.service.erb docs/extended-bash-env.7.md
	mkdir build
	erb -r ./build-config.rb src/extended-bash-env.service.erb > build/extended-bash-env.service
	pandoc -s -t man docs/extended-bash-env.7.md | gzip > build/extended-bash-env.7.gz

install: README.md src/extended-bash-env.pl src/extended-bash-env.sh build/*.service build/extended-bash-env.7.gz
	install -d -m 755 \
		./build/buildroot/usr/local/share/doc/extended-bash-env/ \
		./build/buildroot/usr/local/share/extended-bash-env/  \
		./build/buildroot/usr/local/libexec \
		./build/buildroot/etc/local/extended-bash-env/\
		./build/buildroot/usr/local/share/man/man7/ \
		./build/buildroot/etc/profile.d/ \
		./build/buildroot/etc/systemd/system/ 
	install -m 644 README.md LICENSE ./build/buildroot/usr/local/share/doc/extended-bash-env/
	install -m 755 src/extended-bash-env.pl ./build/buildroot/usr/local/libexec/
	install -m 644 build/extended-bash-env.7.gz ./build/buildroot/usr/local/share/man/man7/
	install -m 644 src/extended-bash-env.sh ./build/buildroot/etc/profile.d/
	install -m 644 build/*.service ./build/buildroot/etc/systemd/system/ 
	#install -m 644 src/*.timer ./build/buildroot/etc/systemd/system/ 

clean:
	rm -rf build
