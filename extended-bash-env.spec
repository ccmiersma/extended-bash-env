%define author Christopher Miersma

Name:		extended-bash-env
Version:        0.0.2
Release:        1.local%{?dist}

Summary:	Extended Bash Environment
Group:		Utilities
License:	MIT
URL:		https://gitlab.com/ccmiersma/%{name}/
Source0:	%{name}-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  pandoc
BuildRequires:  ruby
BuildRequires:  rubygems
Requires:       systemd


%description
Extend the list of environmental variables with hostnamectl

%{!?local_prefix:%define local_prefix %(echo %{release} | cut -f2 -d. | egrep -v "(^el[[:digit:]]|^ol[[:digit:]]|^fc[[:digit:]])" )}


%if "%{local_prefix}" != "" && "%{local_prefix}" != "git"  
  %if "%{local_prefix}" == "opt"  
    %define _prefix /opt
  %else
    %define _prefix /opt/%{local_prefix}
  %endif
  %define _sysconfdir /etc/%{_prefix}
  %define _localstatedir /var/%{_prefix}
  %define _datadir %{_prefix}/share
  %define _docdir %{_datadir}/doc
  %define _mandir %{_datadir}/man
  %define _bindir %{_prefix}/bin
  %define _sbindir %{_prefix}/sbin
  %define _libdir %{_prefix}/lib
  %define _libexecdir %{_prefix}/libexec
  %define _includedir %{_prefix}/include
%endif


%prep
%setup


%build

./configure --prefix=%_prefix --sysconfdir=%_sysconfdir --localstatedir=%_localstatedir --destdir=%buildroot
make

%install


make install




%clean
%__rm -rf ${RPM_BUILD_ROOT}

%files
%defattr(-,root,root, -)
%config(noreplace) /etc/systemd/system/*
%config(noreplace) /etc/profile.d/*
%_libexecdir/*
%_datadir/%{name}
%_docdir/%{name}
%_mandir/man7/*
%docdir %{_mandir}
%docdir %{_docdir}

# The post and postun update the man page database
%post

mandb
systemctl daemon-reload

%postun

mandb
systemctl daemon-reload

%changelog
* Fri Dec 15 2017 Christopher Miersma <ccmiersma@gmail.com> 0.0.2-1.local
- Added profile script. (ccmiersma@gmail.com)
- Fixed systemd service. (ccmiersma@gmail.com)
- Adjusted service file working directory. (ccmiersma@gmail.com)
- Added tito to CI. (ccmiersma@gmail.com)

* Fri Dec 15 2017 Christopher Miersma <ccmiersma@gmail.com> 0.0.1-1.local
- new package built with tito

