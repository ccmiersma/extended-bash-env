# Turn the various files /run/extended-bash-env into environment variables

if test -d /run/extended-bash-env/
then
  for var_file in $(ls /run/extended-bash-env/)
  do
    export $var_file="$(cat /run/extended-bash-env/$var_file)"
  done
fi
